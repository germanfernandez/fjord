/**
 * FUNCIONALIDAD PARA LA PAGINA PRINCIPAL DE FJORD
 
// div contenedor de la imagen de fondo :content_xy8kmyvbg
*/ 


jQuery("#wrapper").css({top:'-90px'});
jQuery("#textoLogoPrincipal").css('background-color', 'red');
jQuery("#content_xy8kmyvbg").hide(0);
jQuery("#content_4qx16f0yh").hide(0);
jQuery("#navbar").hide(0);
jQuery("#navbar-bg").hide(0);


function establecerTextoORD(){
	jQuery("#content_xy8kmyvbg").css({'z-index':99});
	jQuery("#textoLogoPrincipal").css({
		'color':'white'
		});	
	jQuery("#textoLogoPrincipal").html('<div style="cursor:hand;cursor:pointer;font:ostrich;font-weight:bold;" onclick="mostrarHeaderFooter()">'+
									   '<img src="wp-include/images/fjord/fj.svg" />'+
									   '<span id="textoORD" style="color:white;cursor:hand;">ord</span></div>');
	jQuery("#textoORD").hide(0);
	jQuery("#textoORD").fadeIn(5000);
	jQuery("#content_xy8kmyvbg").fadeIn(5000);	
}
function mostrarHeaderFooter(){	
	jQuery("#wrapper").css({top:'30px'});
	jQuery("#navbar").show(1000);
	jQuery("#navbar-bg").show(1000);
	jQuery("#content_4qx16f0yh").show(1000);
	jQuery("#textoLogoPrincipal").hide(0);
}

jQuery(document ).ready(function() {	

	
	
	jQuery("#textoLogoPrincipal").html('<div>'+
			   '<img src="wp-includes/images/fjord/fj.svg" />'+
			   '</div>');	
	jQuery("#textoLogoPrincipal").css("top", Math.max(0, ((jQuery(window).height() - jQuery(this).outerHeight()) / 2) + jQuery(window).scrollTop()) + "px");
	jQuery("#textoLogoPrincipal").css({
				'top':'50%',
				'height': '100%',
				'font-weight':'bold',
				'width':'100%',
				'text-align': 'center',
				'position': 'absolute',
				'font-size': '70px',
				'z-index':100
	});	
	setTimeout(establecerTextoORD,2000);
		
	

    
});
