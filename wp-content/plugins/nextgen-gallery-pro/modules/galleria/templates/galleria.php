<?php $this->start_element('nextgen_gallery.gallery_container', 'container', $displayed_gallery); ?>

<div class="ngg-galleria-parent <?php echo esc_attr($theme); ?>"
     data-id="<?php echo esc_attr($displayed_gallery_id); ?>"
     id="displayed_gallery_<?php echo esc_attr($displayed_gallery_id); ?>"
     style="text-align: center;">
	<div class="ngg-galleria"></div>
	<div class="nggPreviousGalleryLink" id="idNggPreviousGalleryLink" onclick="clickAnteriorGaleria();"></div>
	<div class="nggNextGalleryLink" id="idNggNextGalleryLink" onclick="clickProximaGaleria()"></div>
	<div class="nggNextGalleryLinkTrianguloBottom" id="idNggNextGalleryLink" onclick="clickProximaGaleria()"></div>
	<div class="nggPreviousGalleryLinkBottom" id="idNggNextGalleryLink" onclick="clickAnteriorGaleria()"></div>
</div>
<form id="cambiarGaleria" action="">
<input type="hidden" id="urlCambioGaleria" value="" />
</form>

<?php $this->end_element(); ?>