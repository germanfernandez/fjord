/**
 * FUNCIONALIDAD PARA LA PAGINA PRINCIPAL DE FJORD
 *  // div contenedor de la imagen de fondo :content_xy8kmyvbg
 */

jQuery("#wrapper").css({
	top : '-90px'
});
// jQuery("#masonry-content_4qx16f0yh").css({width:'1170px'});

jQuery("#content_xy8kmyvbg").hide(0);
jQuery("#content_4qx16f0yh").hide(0);
jQuery("#navbar").hide(0);
jQuery("#navbar-bg").hide(0);

function quitarMargen() {
	jQuery("#imgFjBlanco").css({
		'margin-left' : '0px'
	});

}
function mostrarImagenCentral() {

	jQuery("#textoLogoPrincipal")
			.html(
					'<div style="margin-top:20%;">'
							+ '<img id="imgFjNegro" style="height:70px;" src="wp-content/themes/semplice/images/fjord/fj.png" />'
							+ // 100
							'<img id="imgFjBlanco" style="height:70px;display:none;margin-left:-30px;" src="wp-content/themes/semplice/images/fjord/fj-white.png" />'
							+ // 46
							'<img style="height:70px;opacity:0;width:0px;" id="textoORD" src="wp-content/themes/semplice/images/fjord/ord-white.png" />'
							+ '</div>');
	
	jQuery("#content_xy8kmyvbg").fadeIn(2000);
	jQuery("#imgFjNegro").fadeOut(2000);
	jQuery("#imgFjBlanco").fadeIn(2000);
	setTimeout(quitarMargen, 2000);
	setTimeout(transicionOrd, 2000);

}
function transicionOrd() {
	jQuery("#textoORD").animate({
		'width' : '84px'
	}, 2000);
	setTimeout(establecerTextoORD, 2000);
}
function establecerTextoORD() {
	jQuery("#content_xy8kmyvbg").css({
		'z-index' : 99
	});
	jQuery("#textoLogoPrincipal").css({
		'color' : 'white'
	});
	jQuery("#textoLogoPrincipal")
			.html(
					'<div style="margin-top:20%;cursor:hand;cursor:pointer;font:ostrich;font-weight:bold;" onclick="bajarImagen()">'
							+ '<img style="height:70px;" src="wp-content/themes/semplice/images/fjord/fj-white.png" />'
							+ '<img style="height:70px;" id="textoORD" src="wp-content/themes/semplice/images/fjord/ord-white.png" />'
							+ '</div>');

	jQuery("#textoORD").hide(0);
	jQuery("#textoORD").fadeIn(5000);

}
function bajarImagen() {
	jQuery("#wrapper").animate({
		'top' : '0px'
	}, 2000, mostrarHeaderFooter);
	jQuery("#textoLogoPrincipal").fadeOut(2000);
}
function mostrarHeaderFooter() {
	jQuery("#wrapper").css({
		top : '0px'
	});

	jQuery("#divTapon").css({
		visibility : 'visible',
		position : 'relative',
		'background-color' : 'white',
		top : '-30px',
		left : "30%",
		width : '550px',
		height : '80px',
		'z-index' : '999'
	});
	jQuery("#divTapon").fadeOut(2000);
	jQuery("#navbar").show(0);
	jQuery("#navbar-bg").fadeIn(500);
	jQuery("#content_4qx16f0yh").show(0);

}

jQuery(document)
		.ready(
				function() {
                                        
					jQuery("#textoLogoPrincipal")
							.html(
									'<div style="margin-top:20%;">'
											+ '<img id="imgFjNegro" style="height:70px;" src="wp-content/themes/semplice/images/fjord/fj.png" />'
											+ '<img id="imgFjBlanco" style="height:70px;display:none;margin-left:-30px;" src="wp-content/themes/semplice/images/fjord/fj-white.png" />'
											+ '<img style="height:70px;opacity:0;width:0px;" id="textoORD" src="wp-content/themes/semplice/images/fjord/ord-white.png" />'
											+ '</div>');

					jQuery("#textoLogoPrincipal").css(
							"top",
							Math.max(0,
									((jQuery(window).height() - jQuery(this)
											.outerHeight()) / 2)
											+ jQuery(window).scrollTop())
									+ "px");
					jQuery("#textoLogoPrincipal").css({
						'height' : '100%',
						'font-weight' : 'bold',
						'width' : '100%',
						'text-align' : 'center',
						'position' : 'absolute',
						'font-size' : '70px',
						'z-index' : 100
					});
					setTimeout(mostrarImagenCentral, 2500);

				});
