<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'fjeld');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'german');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'GfD7!#1T*A9qpk@+;XM;Xe#!4srm;F:dLtYW)n%VMi6xroiD7fdc~PV9m%?yD7RU');
define('SECURE_AUTH_KEY',  '*%y8dy6!1L2TT`o76mjFV3s!cZ9BN#+ssO^)X_7K*52"QSH1t*00o~ahwdNO_|z9');
define('LOGGED_IN_KEY',    '#Yq$oOAqTiEvPd9"|AM5k)NY~MaP~eYeUogoZfwtY^Ea)h^PO~DQ|C&2H|k?mxt|');
define('NONCE_KEY',        'ENTTuo"r(DW;78kHjfWFb7@ziN|xO:WQZY7ku:(lr21Vo2S8ZlCAoH8bOhbDf/ME');
define('AUTH_SALT',        '1o`wJl@STGbTV;^(yIK|7*$y6CS&8W6gG7x9&1jr23D8_Z/WfKr"zVy(0?"PuaAT');
define('SECURE_AUTH_SALT', 'yFUQ/Y7q6Sa5fb0&TjL#5o^~uKjyFgJ4i@k2!a)ZhMZZhYW5h_p~oqjxdJd%X^SC');
define('LOGGED_IN_SALT',   'gDs"#8RtBb*Wj)/BU#_Tah+~SdBOo5Ar~Fi/d|3"KQ039wk%+Jn:F;7nUkyH("Rk');
define('NONCE_SALT',       'n"bT+OxRlylLy5$7HLhZICL(JDN1V;wo+o9HDJPSLN%fL07CRS_b(l@WQaI+oPbu');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_hdcxit_';

/**
 * Limits total Post Revisions saved per Post/Page.
 * Change or comment this line out if you would like to increase or remove the limit.
 */
define('WP_POST_REVISIONS',  10);

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

